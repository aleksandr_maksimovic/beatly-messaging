import React from 'react';
import { mount } from 'enzyme';
import App from '../../App';
import MessageComposer from './message-composer';
import senders from '../../senders.json'
import 'jest-localstorage-mock';


const now = Date.now();
Date.now = jest.genMockFunction().mockReturnValue(now);

const formatTime = (utcTime) => {
  const dateObj = new Date(utcTime);
  let hours = dateObj.getHours();
  let minutes = dateObj.getMinutes();
  const meridiem = (hours > 11) ? "pm" : "am";
  const day = dateObj.getDay();
  const month = dateObj.getDate();

  if (hours > 12) {
    hours -= 12;
  }
  if (hours === 0) {
    hours = "12";
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  return `${day}/${month} at ${hours}:${minutes}${meridiem}`
}

describe("App component", () => {
  let props;
  let mountedApp;
  const app = () => {
    if (!mountedApp) {
      mountedApp = mount(
          <App
            {...props}
          />
      );
    }
    return mountedApp;
  }

  beforeEach(() => {
    props = {};
    mountedApp = undefined;
  });

  it("renders <MessageComposer />", () => {
    const application = app();
    const MessageComposerComponent = app().find(MessageComposer);
    expect(MessageComposerComponent.length).toBeGreaterThan(0);
  });

  it("renders 3 senders to message sender selector", () => {
    const application = app();
    const MessageComposerComponent = app().find(MessageComposer);
    const senderSelector = MessageComposerComponent.find('select.message-sender-selector');
    expect(senderSelector.find('option').length).toBe(3);
  });

  it("renders default selector value correctly ", () => {
    const application = app();
    const MessageComposerComponent = app().find(MessageComposer);
    const senderSelector = MessageComposerComponent.find('select.message-sender-selector');
    expect(senderSelector.props().value).toBe("1");
  });

  it("should have disabled submit button while no message are written to textarea", () => {
    const application = app();
    const submitButton = app().find('button.button.button--primary');
    expect(submitButton.props().disabled).toBe(true);
  });

  it("should not have disabled submit button while message is present in textarea", () => {
    const application = app();
    application.setState({ message: "Message" });
    const submitButton = app().find('button.button.button--primary');
    expect(submitButton.props().disabled).toBe(false);
  });

  it("render a newly submited message", () => {
    const application = app();
    application.setState({ message: "Message" });
    const submitButton = app().find('button.button.button--primary');
    submitButton.simulate('click');
    const timestamp = Date.now();

    let messageb = [{"id": 1, "message": "Message", "sender": 1}];
    messageb[0].timestamp = timestamp;

    expect(application.state().chatLog).toEqual(messageb);
    expect(application.state().message).toEqual("");
    expect(application.find('span.message-sender').text()).toEqual("Alex | Beatly");
    expect(application.find('div.message-content').text()).toEqual("Message");
    expect(application.find('span.message-timestamp').text()).toEqual(formatTime(now));
  });

});
