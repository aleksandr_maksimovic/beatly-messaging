import React from 'react';
import PropTypes from 'prop-types';

import './message-composer-controls.css';

const MessageComposerControls = props =>(
  <div className="message-composer-controls">
    <div className="message-sender-wrapper">
      Send as: &nbsp;
      <select value={props.sender} onChange={props.onSenderChange} className="message-sender-selector">
        {props.senders.map(sender => (
          <option key={sender.id} value={sender.id}>{sender.name}</option>
        ))}
      </select>
    </div>
    <button type="submit" disabled={!props.message} onClick={props.onMessageSubmit} className="button button--primary">Send</button>
  </div>
)

export default MessageComposerControls;

MessageComposerControls.propTypes = {
  message: PropTypes.string.isRequired,
  onMessageSubmit: PropTypes.func.isRequired,
  sender: PropTypes.string.isRequired,
  senders: PropTypes.arrayOf(
      PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      group: PropTypes.string.isRequired
    })
  )

}
