import React from 'react';
import PropTypes from 'prop-types';

import './message-composer.css';

import MessageComposerControls from './message-composer-controls';

const MessageComposer = props =>(
  <div className="message-composer-wrapper">
    <textarea
      value={props.message}
      onChange={props.onMessageChange}
      placeholder="Enter your message..."
      rows="1"
      className="message-composer"/>
    <MessageComposerControls
      message={props.message}
      sender={props.sender}
      senders={props.senders}
      onSenderChange={props.onSenderChange}
      onMessageSubmit={props.onMessageSubmit}
    />
  </div>
)

export default MessageComposer;

MessageComposer.propTypes = {
  message: PropTypes.string.isRequired,
  onMessageChange: PropTypes.func.isRequired,
  sender: PropTypes.string.isRequired,
  senders: PropTypes.arrayOf(
      PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      group: PropTypes.string.isRequired
    })
  )
}
