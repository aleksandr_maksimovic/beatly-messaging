import React from 'react';
import PropTypes from 'prop-types';
import ReactEmoji from 'react-emoji';


import './message.css';

const Message = props => (
  <div key={props.message.id} className="message-wrapper">
    <div className="message-topbar">
      <span className="message-sender">
        {props.getSender(props.message.sender).name} | {props.getSender(props.message.sender).group}
      </span>
      <span className="message-timestamp">
        {props.formatTime(props.message.timestamp)}
      </span>
    </div>
    <div className="message-content">
      {ReactEmoji.emojify(props.message.message)}
    </div>
  </div>
)

export default Message;

Message.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.number.isRequired,
    sender: PropTypes.number.isRequired,
    timestamp: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired
  }).isRequired
}
