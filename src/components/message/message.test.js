import React from 'react';
import { mount } from 'enzyme';
import Message from './../message/message';
import chatlog from '../../chatlog.json';
import senders from '../../senders.json';

describe("Message component", () => {
  let props;
  let mountedMessages;

  const getSender = () => {
    return senders[0]
  }

  const formatTime = (utcTime) => {
    const dateObj = new Date(utcTime);
    let hours = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    const meridiem = (hours > 11) ? "pm" : "am";
    const day = dateObj.getDay();
    const month = dateObj.getDate();

    if (hours > 12) {
      hours -= 12;
    }
    if (hours === 0) {
      hours = "12";
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return `${day}/${month} at ${hours}:${minutes}${meridiem}`
  }

  const message = chatlog[0];

  const messageComponent = () => {
    if (!mountedMessages) {
      mountedMessages = mount(
          <Message
            key={message.id}
            message={message}
            getSender={getSender}
            formatTime={formatTime}
            {...props}
          />
      );
    }
    return mountedMessages;
  }

  beforeEach(() => {
    props = {
    };
    mountedMessages = undefined;
  });

  it("renders message's sender name and group correctly", () => {
    const sender = messageComponent().find('span.message-sender').first();
    const senderName = `Alex | Beatly`;
    expect(sender.text()).toEqual(senderName);
  });

  it("renders message time correctly", () => {
    const timestamp = messageComponent().find('span.message-timestamp').first();
    const time = `2/16 at 8:29pm`;
    expect(timestamp.text()).toEqual(time);
  });

  it("renders message content correctly", () => {
    const content = messageComponent().find('div.message-content').first();
    const text = `Message #1`;
    expect(content.text()).toEqual(text);
  });
})
