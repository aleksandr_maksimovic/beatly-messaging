import React from 'react';
import { mount } from 'enzyme';
import Message from './../message/message';
import MessageList from './message-list';
import chatlog from '../../chatlog.json';
import senders from '../../senders.json';

const formatTime = (utcTime) => {
  const dateObj = new Date(utcTime);
  let hours = dateObj.getHours();
  let minutes = dateObj.getMinutes();
  const meridiem = (hours > 11) ? "pm" : "am";
  const day = dateObj.getDay();
  const month = dateObj.getDate();

  if (hours > 12) {
    hours -= 12;
  }
  if (hours === 0) {
    hours = "12";
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  return `${day}/${month} at ${hours}:${minutes}${meridiem}`
}

describe("Message list with chatlog", () => {
  let props;
  let mountedMessages;

  const getSender = () => {
    return senders[0]
  }

  const messageList = () => {
    if (!mountedMessages) {
      mountedMessages = mount(
          <MessageList
            chatLog={chatlog}
            getSender={getSender}
            formatTime={formatTime}
            message=""
            {...props}
          />
      );
    }
    return mountedMessages;
  }

  beforeEach(() => {
    props = {
    };
    mountedMessages = undefined;
  });

  it("renders a <MessageList />", () => {
    const messageListComponent = messageList();
    expect(messageListComponent.length).toBeGreaterThan(0);
  });

  it("renders 3 messsages inside <MessageList /> from mocked json", () => {
    const messageListComponent = messageList().find(Message);
    const messages = messageListComponent.find('div.message-wrapper');
    expect(messages).toHaveLength(3);
  });

});

describe("Message list without chatlog", () => {
  let props;
  let mountedMessages;

  const getSender = () => {
    return senders[0]
  }

  const messageList = () => {
    if (!mountedMessages) {
      mountedMessages = mount(
          <MessageList
            getSender={getSender}
            formatTime={formatTime}
            message=""
            {...props}
          />
      );
    }
    return mountedMessages;
  }

  beforeEach(() => {
    props = {
    };
    mountedMessages = undefined;
  });

  it("it does not render a <MessageList /> if there is no chatlog", () => {
    const messageListComponent = messageList().find(Message);
    const messages = messageListComponent.find('div.message-wrapper');
    expect(messages).toHaveLength(0);
  });

});
