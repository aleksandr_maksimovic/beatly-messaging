import React from 'react';
import PropTypes from 'prop-types';

import Message from '../message/message';


const MessageList = (props) => {
  if (props.chatLog) {
    return (
      props.chatLog.map(message => (
        <Message
          key={message.id}
          message={message}
          getSender={props.getSender}
          formatTime={props.formatTime}
        />
      ))
    )
  }
  return (null);
};

export default MessageList;

MessageList.propTypes = {
  chatLog: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      sender: PropTypes.number.isRequired,
      timestamp: PropTypes.number.isRequired,
      message: PropTypes.string.isRequired
    }).isRequired,
  ),
  getSender: PropTypes.func.isRequired,
  formatTime: PropTypes.func.isRequired,

}
