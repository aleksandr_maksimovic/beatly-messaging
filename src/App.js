import React, { Component } from 'react';
import './App.css';

import MessageList from './components/message-list/message-list';
import MessageComposer from './components/message-composer/message-composer';
import senders from './senders.json';

class App extends Component {

  getChatLog = () => JSON.parse(localStorage.getItem('chatLog')) || [];
  getSenderId = () => JSON.parse(localStorage.getItem('senderId')) || "1";

  onMessageChange = (event) => {
    this.setState({message: event.target.value});
  }

  onSenderChange = (event) => {;
    localStorage.setItem('senderId', JSON.stringify(event.target.value));
    this.setState({sender: event.target.value});
  }

  getSender = (senderID) => {
    for (let sender of this.state.senders) {
      if(sender.id === senderID){
        return sender
      }
    }
  }

  generateMessageId = () => {
    if (this.state.chatLog.length === 0){
      return 1
    }
    return this.state.chatLog[this.state.chatLog.length-1].id+1
  }

  updateChatLog = (chatLog) => {
    localStorage.setItem('chatLog', JSON.stringify(chatLog));
    this.setState({chatLog: chatLog});
  }

  onMessageSubmit = () => {
    const newMessage = [
      {
        "id": this.generateMessageId(),
        "sender": this.getSender(Number(this.state.sender)).id,
        "timestamp": Date.now(),
        "message": this.state.message
      }
    ]
    const updatedChatLog = this.state.chatLog.concat(newMessage);
    this.updateChatLog(updatedChatLog);
    this.setState({message: ""});
  };

  formatTime = (utcTime) => {
    const dateObj = new Date(utcTime);
    let hours = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    const meridiem = (hours > 11) ? "pm" : "am";
    const day = dateObj.getDay();
    const month = dateObj.getDate();

    if (hours > 12) {
      hours -= 12;
    }
    if (hours === 0) {
      hours = "12";
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return `${day}/${month} at ${hours}:${minutes}${meridiem}`
  }

  state = {
    chatLog: this.getChatLog(),
    message: "",
    sender: this.getSenderId(),
    senders: senders
  }

  render() {
    return (
      <div className="messaging-widget">
        <MessageList
          chatLog={this.state.chatLog}
          getSender={this.getSender}
          formatTime={this.formatTime}
        />
        <MessageComposer
          message={this.state.message}
          sender={this.state.sender}
          senders={this.state.senders}
          onSenderChange={this.onSenderChange}
          onMessageChange={this.onMessageChange}
          onMessageSubmit={this.onMessageSubmit}
        />
      </div>
    );
  }
}

export default App;
