## Install
From root directory run `npm install` or `yarn install`, depends on which package manager you are using.

## Run app in development mode
Run `npm start` or `yarn start`.
App should open automatically in your browsers new tab. If not, open http://localhost:3000 to view it in the browser.
## Run test
`npm test` or `yarn test`
Runs the test watcher in an interactive mode.
By default, runs tests related to files changed since the last commit.


# Work Sample Beatly for Frontend Developer
Beatly connects brands with people that are considered being influencers. The influencer then spreads the word about the brand in one way or another. Mostly through Instagram, but in the long run probably any platform.

The purpose of this work sample is for you to show some basic knowledge about technologies used client side in web development, such as CSS, HTML and JavaScript.

## The task
Your mission is to build a simple messaging UI that we will use to communicate with the influencer.

* The UI will consist of a list of messages ordered by date/time written, much alike the Facebook inbox
* Besides the list of messages, make it possible to choose the sender when posting a message. Possible senders are “Jonas at Beatly”, “Alex at Beatly” and “Zlatan Influencer”.
The messages that are posted should appear in the list. Here’s a mockup: http://bit.ly/1quD51A

* Use any (or no) framework(s)
* Keep it simple
* Keep the code clean
* Skip the server side work, demonstrate the functionality only by building the client side (you can store things in a global variable or localStorage or any way you prefer client side)
* Use git the way you usually do when developing and make sure I have access to it (jonas.arnklint@beatly.com). You can use github or bitbucket or any other platform for git.

Please email any questions to jonas.arnklint@beatly.com, I’ll try to answer them ASAP.
